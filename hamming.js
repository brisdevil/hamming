﻿var buttonFile = document.querySelector("#file"),
	buttonStart = document.querySelector("input[type='button']"),
	fieldInput = document.querySelector(".input"),
	fieldOutput = document.querySelector(".output"),
	file, str = '';

document.onmousedown = function() {
	return false;
}
/*чтение из файла в переменную str*/
buttonFile.onchange = function(event) {
	var reader = new FileReader(),
		text,
		matchResult;
	file = this.files[0];
	reader.onload = function(event) {
		str = '';
		text = this.result;
		console.log(text);
		matchResult = text.match(/[01]+/);
		if (matchResult == null || matchResult[0] != text)
			for (var i = 0; i < text.length; i++)
				str += text.charCodeAt(i).toString(2);
		else str = text;
		fieldInput.innerHTML = str;
		fieldOutput.innerHTML = '-';
	};
	if (file.type == "text/plain")
			reader.readAsText(file);
	else 
		throw new Error("Нетекстовый файл");
};
 
/*клик по кнопке Поехали*/
buttonStart.onclick = function(event) {
	var input = document.querySelector("input[name='mode']:checked"),
		result,
		resultStr = '';
	if (!file) 
		throw new Error("Файл не получен!");
	switch (input.value) {
		case 'code':
			result = code(str);
			for (var i = 0; i < result.length; i++)
	  			if (parseInt(Math.log(i+1) / Math.log(2)) == Math.log(i+1) / Math.log(2))
					resultStr += '<span class="control">' + result[i] + '</span>';
				else resultStr += result[i];
			fieldOutput.innerHTML = resultStr;
			break;
		case 'decode':
			fieldOutput.innerHTML = decode(str);
	}
};
 
/*функция для кодирования*/
function code(string) {
	var encodedBitsString,
		controlBitsCount = calculateControlBits(string),
		bitsArray = string.split(''),
		hammingMatrix,
		multiplyResults;
	bitsArray = bitsArray.map(function(x) {
	  return +x;
	});
	insertControlBits(controlBitsCount, bitsArray);
	hammingMatrix = createHamArray(bitsArray, controlBitsCount);
	multiplyResults = multiplyStrings(hammingMatrix, bitsArray);
	insertCodingBits(bitsArray, multiplyResults);
	return bitsArray.join('');
}
 
 
/*функция для декодирования*/
function decode(string) {
	var decodedBitsString,
		controlBitsCount = calculateControlBits(string),
		bitsArray = string.split(''),
		hammingMatrix,
		multiplyResults,
		errorNumber,
		temp;
	bitsArray = bitsArray.map(function(x) {
	  return +x;
	});
	hammingMatrix = createHamArray(bitsArray, controlBitsCount);
	multiplyResults = multiplyStrings(hammingMatrix, bitsArray);
	errorNumber = checkResults(bitsArray, multiplyResults);
	if (errorNumber) {
		temp = fieldInput.innerHTML;
		fieldInput.innerHTML = temp.substring(0, errorNumber);
		fieldInput.innerHTML += '<span class="control">' + temp[errorNumber] + '</span>';
		fieldInput.innerHTML += temp.substring(errorNumber + 1);
		return ('Ошибка на позиции ' + errorNumber);
	}
	decodeBitsArray(bitsArray, controlBitsCount);
	return bitsArray.join('');
}
 
/*функция для вычисления количества
контрольных битов, необходимых для вставки
в исходную строку*/
/*на вход - исходная строка*/
function calculateControlBits(str) {
	var bitsCount = str.length,
		controlBitsCount = 1,
		insertNumber; 
	do {
		insertNumber = Math.ceil( Math.log(bitsCount + controlBitsCount) / Math.log(2) );
		controlBitsCount++;
	} while (insertNumber != controlBitsCount);
	return controlBitsCount;
}
 
/*функция для вставки контрольных битов в исходную строку*/
/*на вход - количество контрольных битов,
изначальный битовый массив*/
function insertControlBits(controlBitsCount, bitsArray) {
	for (var i = 0; i < controlBitsCount; i++)
	  bitsArray.splice(Math.pow(2, i) - 1, 0, 0);
}
 
/*функция, генерирующая матрицу Хэмминга*/
/*на вход - массив с исходной кодировкой и контрольными битами,
количество этих самых контрольных битов*/
function createHamArray(bitsArray, controlBitsCount) {
	var i, j, temp, bit,
		hammingMatrix = [];
	for (i = 0; i < controlBitsCount; i++) {
		hammingMatrix[i] = [];
	}
	for (j = 0; j < bitsArray.length; j++) {
		temp = (j+1).toString(2);
		temp = temp.split('').reverse().join('');
		for (i = 0; i < controlBitsCount; i++) {
			bit = +temp[i] ? 1 : 0;
			hammingMatrix[i][j] = bit;
		}
	}
	return hammingMatrix;
}
 
/*функция, находящая r[0..controlBitsCount]*/
/*на вход - матрица Хемминга,
исходная строка с нулевыми контрольными битами*/
function multiplyStrings(hammingMatrix, bitsArray) {
	var i, j,
		multiplyResults = [];
	for (i = 0; i < hammingMatrix.length; i++) {
		multiplyResults[i] = 0;
		for (j = 0; j < hammingMatrix[i].length; j++) {
			multiplyResults[i] += hammingMatrix[i][j] * bitsArray[j];
		}
		multiplyResults[i] = multiplyResults[i] % 2;
	}
	return multiplyResults;
}
 
/*функция, заменяющая прежде исключительно нулевые контрольные биты
значениями, полученными в функции multiplyStrings*/
/*на вход - массив битов с нулевыми контрольными битами,
массив r[0..i]*/
function insertCodingBits(bitsArray, multiplyResults) {
	for (var i = 0; i < multiplyResults.length; i++)
		bitsArray[Math.pow(2, i) - 1] = multiplyResults[i];
}
 
/*проверяет результат и возвращает позицию ошибки*/
/*на вход - массив битов с нулевыми контрольными битами,
массив r[0..i]*/
function checkResults(bitsArray, multiplyResults) {
	return parseInt(multiplyResults.join(''), 2);
}
 
/*декодирует строку*/
/*на вход - закодированный массив битов,
кол-во контрольных битов*/
function decodeBitsArray(bitsArray, controlBitsCount) {
	for (var i = controlBitsCount; i >= 0; i--)
	  bitsArray.splice(Math.pow(2, i) - 1, 1);
}